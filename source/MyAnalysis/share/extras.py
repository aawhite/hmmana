
def adjustStringLength(s,l):
    # adjust length of string until it is l-long
    s=str(s)
    while len(s)<l:
        s="0"+s
    return s

def niceTime():
    import time
    l = 2
    year = adjustStringLength(time.gmtime().tm_year,4)
    year = "18"
    month = adjustStringLength(time.gmtime().tm_mon,l)
    day = adjustStringLength(time.gmtime().tm_mday,l)
    hour = adjustStringLength(time.gmtime().tm_hour,l)
    minute = adjustStringLength(time.gmtime().tm_min,l)
    return minute+hour+"."+day+month+year

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[ 3 3 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[ 3 1 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[ 3 2 m{0}\033[ 3 9 m".format(" ".join(string))
    return ret

