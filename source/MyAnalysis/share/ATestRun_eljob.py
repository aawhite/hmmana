#!/usr/bin/env python
import os, glob
from AnaAlgorithm.DualUseConfig import addPrivateTool

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
(options,args) = parser.parse_args()

dest = options.submission_dir
if dest!="": os.popen("rm -r {0}".format(dest))

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree','CollectionTree')
inputFilePath = os.getenv('ALRB_TutorialData') + '/r9315/'
# inputFilePath = "../data/18_13TeV.00363830.physics_Main.deriv.DAOD_EXOT0.f1002_m2037_p3583_tid15759654_00/"
inputFilePath = "/eos/home-a/aawhite/dilep/data18_13TeV.00363830.physics_Main.deriv.DAOD_EXOT0.f1002_m2037_p3583_tid15759654_00/"
ROOT.SH.ScanDir().filePattern("DAOD_EXOT0.15759654._000252.pool.root.1").scan(sh,inputFilePath)
# ROOT.SH.ScanDir().filePattern("*root*").scan(sh,inputFilePath)
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler(sh)
job.options().setDouble(ROOT.EL.Job.optMaxEvents,10000)

# Creatonrithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ('MyxAODAnalysis','AnalysisAlg')

doMuons = False
doMuons = True

##################################
# configure algorithem properties here!
##################################
# alg.OutputLevel = 2 # 2=DEBUG, comment for info-only
alg.selectOnMuons = doMuons

##################################
# configure GRL
##################################
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
fullGRLFilePath = glob.glob("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/*/*.xml")
alg.grlTool.GoodRunsListVec=fullGRLFilePath
alg.grlTool.PassThrough=True # if true (default) will ignore result of GRL and will just pass all events
alg.doGrl = True
alg.doGrl = False

##################################
# configure Trigger
##################################
alg.doTrigger = True
# Triggers: https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled#Egamma%20(Updated%20on%205%20May%202018)
if doMuons:
    alg.triggerPattern = "HLT_mu50 || HLT_mu26_ivarmedium"
else:
    alg.triggerPattern = "HLT_2e24_lhvloose_nod0"

##################################
# configure electrons
##################################
# Can be: LooseTrackOnly,Loose,Gradient,GradientLoose,FixedCutTight,FixedCutTightTrackOnly
alg.elecWp = "Tight"
alg.elecMaxEta = 2.5

##################################
# configure Muons
##################################
alg.muonMaxEta = 2.5
alg.muonWp = "Loose"

##################################
# configure Dilepton selection
##################################
alg.dilepMassLow = 80 # GeV
alg.dilepMassHigh = 160 # GeV

##################################
# tree output
##################################
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Add our algorithm to the job
job.algsAdd(alg)

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit(job,options.submission_dir)
