#!/usr/bin/env python
import os, glob, sys
sys.path = [os.path.realpath(__file__)] + sys.path
from extras import *
import ROOT
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))
from ROOT import xAOD
from ROOT import SH
from ROOT import EL
### set up the job for xAOD access
xAOD.Init().ignore()


# sys.path=["/afs/cern.ch/user/a/aawhite/panda"]+sys.path
from AnaAlgorithm.DualUseConfig import addPrivateTool

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
(options,args) = parser.parse_args()

dest = options.submission_dir
if dest!="": os.popen("rm -r {0}".format(dest))


# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
sh = SH.SampleHandler()
sh.setMetaString('nc_tree','CollectionTree')
# SH.scanRucio(sh,"data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_ZMUMU.repro21_v01/");
# SH.scanRucio(sh,"data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_ZMUMU.repro21_v01/");
SH.scanRucio(sh,"data18_13TeV.*.physics_Main.PhysCont.DAOD_EXOT0.grp18_v01_p3583");
sh.printContent()

# Create an EventLoop job.
job = EL.Job()
job.sampleHandler(sh)
# job.options().setDouble(EL.Job.optMaxEvents,2000)

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ('MyxAODAnalysis','AnalysisAlg')
doMuons = False
doMuons = True
leptonName=["elec","muon"][doMuons]
timeName = niceTime()

##################################
# configure algorithem properties here!
##################################
# alg.OutputLevel = 2 # 2=DEBUG, comment for info-only
alg.selectOnMuons = doMuons


##################################
# configure GRL
##################################
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
fullGRLFilePath = glob.glob("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/*/*.xml")
alg.grlTool.GoodRunsListVec=fullGRLFilePath
alg.grlTool.PassThrough=True # if true (default) will ignore result of GRL and will just pass all events
alg.doGrl = True

##################################
# configure Trigger
##################################
# Triggers: https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled#Egamma%20(Updated%20on%205%20May%202018)
alg.doTrigger = True
if doMuons:
    alg.triggerPattern = "HLT_mu50 || HLT_mu26_ivarmedium"
else:
    alg.triggerPattern = "HLT_2e24_lhvloose_nod0"

##################################
# configure electrons
##################################
# Can be: LooseTrackOnly,Loose,Gradient,GradientLoose,FixedCutTight,FixedCutTightTrackOnly
alg.elecWp = "Tight"
alg.elecMaxEta = 2.5

##################################
# configure Muons
##################################
alg.muonMaxEta = 2.5
alg.muonWp = "Loose"

##################################
# configure Dilepton selection
##################################
alg.dilepMassLow = 80 # GeV
alg.dilepMassHigh = 160 # GeV

##################################
# launch job
##################################
# Add our algorithm to the job
job.outputAdd(EL.OutputStream('ANALYSIS'))
job.algsAdd(alg)

import time
driver = EL.PrunDriver()
# driver.options().setString("nc_outputSampleName","user.aawhite.abcd.%in:name[2]%.%in:name[6]%");
outputSampleName = "user.aawhite.hmm.{0}.{1}.%in:name[2]%.%in:name[6]%".format(leptonName,timeName)
driver.options().setString("nc_outputSampleName",outputSampleName);
driver.options().setString('nc_optGridNfilesPerJob', '5')
# driver.options().setString("nc_outputSampleName", "user.aawhite.ykcdabcd.%in:name[2]%.%in:name[6]%");
print "="*50
print driver
print job
print options
print outputSampleName
print "="*50
print outputSampleName
driver.submitOnly(job,options.submission_dir)


# driver.options().setString(EL.Job.optSubmitFlags, '--forceStaged')
# driver.options().setString(EL.Job.optSubmitFlags, '--allowTaskDuplication')
# driver.options().setString(EL.Job.optGridNGBPerJob, '5')
# driver.options().setString('nc_optGridNfilesPerJob', '5')
# driver.options().setString('nc_outputSampleName', "testNameSample")
# driver.submitOnly(job,options.submission_dir)

print "DONE"*20
