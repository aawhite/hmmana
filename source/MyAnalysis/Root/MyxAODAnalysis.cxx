#include <TSystem.h>
#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <TLorentzVector.h>
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

//ismet
// #include "CxAODMaker/ElectronHandler.h"
#include "AsgTools/AnaToolHandle.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonSelectorTools/LikelihoodEnums.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"

// for holding electorons
#include "xAODEgamma/ElectronContainer.h" // not needed?
#include <xAODCore/ShallowCopy.h> // from ABtutorial

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_grl ("GoodRunsListSelectionTool/grl", this),
    m_leptonSelection("CP::IsolationSelectionTool",this),
    m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool")
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
    nEvents=0;
    nEventsPass=0;
    declareProperty( "dilepMassLow", m_dilepMassLow = 80,
                   "Lower bound Dilepton mass" );
    declareProperty( "dilepMassHigh", m_dilepMassHigh= 160,
                   "Higher bound Dilepton mass" );
    declareProperty( "selectOnMuons", m_selectOnMuons= true,
                   "Flag for whether to select events on muons or electrons" );
    declareProperty( "doGrl", m_doGrl= true,
                   "Do grl flag" );
    declareProperty( "doTrigger", m_doTrigger= true,
                   "Do trigger flag" );

    // ############################################################################
    // GRL
    // ############################################################################
    // declare the tool handle as a property on the algorithm
    m_grl.declarePropertyFor (this, "grlTool");

    // ############################################################################
    // Electrons
    // ############################################################################
    declareProperty( "elecWp", m_elecWp="Loose",
                   "Value for property elec WorkingPoint" );
    declareProperty( "elecMaxEta", m_elecSelectionMaxEta=2.5,
                   "Value for property elec Max Eta" );

    // ############################################################################
    // Muon
    // ############################################################################
    declareProperty( "muonWp", m_muonWp= "Loose",
                   "WP for muon" );
    declareProperty( "muonMaxEta", m_muonSelectionMaxEta=2.5,
                   "Value for property Muon Max Eta" );

    // ############################################################################
    // Trigger tools
    // ############################################################################
    declareProperty( "triggerPattern", m_triggerPattern = "Unknown",
                   "Pattern for matching trigger" );


}


StatusCode MyxAODAnalysis :: initialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.
    ANA_MSG_DEBUG ("in initialize");
    // ############################################################################
    // Configure GRL (can also be done from python)
    // const char* GRLFilePath = "$ALRB_TutorialData/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
    // const char* fullGRLFilePath = gSystem->ExpandPathName(GRLFilePath);
    // std::vector<std::string> vecStringGRL;
    // vecStringGRL.push_back(fullGRLFilePath);
    // ANA_CHECK(m_grl.setProperty( "GoodRunsListVec", vecStringGRL));
    // ANA_CHECK(m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    ANA_CHECK(m_grl.initialize());
    // ############################################################################

    // ############################################################################
    // Trigger tools
    // ############################################################################
    ANA_CHECK (m_trigConfigTool.initialize());
    ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
    ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
    ANA_CHECK (m_trigDecisionTool.initialize());

    // ############################################################################
    // Lepton selection tool
    // ############################################################################
    ANA_CHECK (m_leptonSelection.setProperty("ElectronWP", m_elecWp));
    ANA_CHECK (m_leptonSelection.setProperty("MuonWP", m_muonWp));
    ANA_CHECK (m_leptonSelection.initialize());

    // ############################################################################
    // Book histograms
    ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]
    ANA_CHECK (book (TH1F ("h_elecPt", "h_elecPt", 100, 0, 500))); // elec pt [GeV]
    ANA_CHECK (book (TH1F ("h_elecEta", "h_elecEta", 100, -3, 3)));
    ANA_CHECK (book (TH1F ("h_elecPhi", "h_elecPhi", 100, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_muonPt", "h_muonPt", 100, 0, 500))); // muon pt [GeV]
    ANA_CHECK (book (TH1F ("h_muonEta", "h_muonEta", 100, -3, 3)));
    ANA_CHECK (book (TH1F ("h_muonPhi", "h_muonPhi", 100, -3.2, 3.2)));
    ANA_CHECK (book (TH1F ("h_cutflow", "h_cutflow", 10, 0, 10)));
    // ############################################################################

    // ############################################################################
    // Trees
    // ############################################################################
    ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
    TTree* mytree = tree ("analysis");
    // make vectors
    m_jetEta = new std::vector<float>();
    m_jetPhi = new std::vector<float>();
    m_jetPt = new std::vector<float>();
    m_jetE = new std::vector<float>();
    m_elecEta = new std::vector<float>();
    m_elecPhi = new std::vector<float>();
    m_elecPt = new std::vector<float>();
    m_elecE = new std::vector<float>();
    m_elecQ = new std::vector<float>();
    m_muonEta = new std::vector<float>();
    m_muonPhi = new std::vector<float>();
    m_muonPt = new std::vector<float>();
    m_muonE = new std::vector<float>();
    m_muonQ = new std::vector<float>();
    // set branches
    mytree->Branch("RunNumber", &m_runNumber);
    mytree->Branch("EventNumber", &m_eventNumber);
    mytree->Branch("jetEta", &m_jetEta);
    mytree->Branch("jetPhi", &m_jetPhi);
    mytree->Branch("jetPt", &m_jetPt);
    mytree->Branch("jetE", &m_jetE);
    mytree->Branch("elecEta", &m_elecEta);
    mytree->Branch("elecPhi", &m_elecPhi);
    mytree->Branch("elecPt", &m_elecPt);
    mytree->Branch("elecE", &m_elecE);
    mytree->Branch("elecQ", &m_elecQ);
    mytree->Branch("muonEta", &m_muonEta);
    mytree->Branch("muonPhi", &m_muonPhi);
    mytree->Branch("muonPt", &m_muonPt);
    mytree->Branch("muonE", &m_muonE);
    mytree->Branch("muonQ", &m_muonQ);


    return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.
    // retrieve the eventInfo object from the event store
    nEvents+=1;
    if (nEvents%1000==0) ANA_MSG_INFO("Event: "<<nEvents<<" passed: "<<nEventsPass);
    int cutflowCount = 0;
    hist("h_cutflow")->Fill(++cutflowCount);
    hist("h_cutflow")->GetXaxis()->SetBinLabel(cutflowCount,"DxAOD");
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve(eventInfo, "EventInfo"));
    // print out run and event number from retrieved object
    ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    // Read/fill the EventInfo variables:
    m_runNumber = eventInfo->runNumber();
    m_eventNumber = eventInfo->eventNumber();

    // ############################################################################
    // GRL
    // ############################################################################
    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    bool isMC = false;
    // check if the event is MC
    if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
        isMC = true;
    }

    // if data check if event passes GRL
    if (m_doGrl){
        if (!isMC) { // it's data!
            if (!m_grl->passRunLB(*eventInfo)) {
                ANA_MSG_DEBUG ("Skipped event on account of GRL");
                return StatusCode::SUCCESS; // go to next event
            }
        }
        hist("h_cutflow")->Fill(++cutflowCount); // GRL
        hist("h_cutflow")->GetXaxis()->SetBinLabel(cutflowCount,"Pass-GRL");
    }

    // ############################################################################
    // Trigger
    // ############################################################################
    // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    if (m_doTrigger){
        ANA_MSG_DEBUG ("Checking trigger, "<<m_triggerPattern);
        auto chainGroup = m_trigDecisionTool->getChainGroup(m_triggerPattern); // can use wildcard in format, eg: "HLT_xe80.*"
        std::map<std::string,int> triggerCounts;
        bool passTrig = false; // see if any matching triggers found
        for(auto &trig : chainGroup->getListOfTriggers()) {
            ANA_MSG_DEBUG ("Checking trig="<<trig);
            auto cg = m_trigDecisionTool->getChainGroup(trig);
            std::string thisTrig = trig;
            ANA_MSG_DEBUG ("execute(): " << thisTrig << ", chain passed(1)/failed(0) = " << cg->isPassed() << ", total chain prescale (L1*HLT) = " << cg->getPrescale());
            passTrig|=cg->isPassed();
        }
        // if an event does not pass a trigger, skip this
        if (!passTrig){
            ANA_MSG_DEBUG ("Skipped event on account of no trigger");
            return StatusCode::SUCCESS; // go to next event
        }
        else {
            hist("h_cutflow")->Fill(++cutflowCount);
            hist("h_cutflow")->GetXaxis()->SetBinLabel(cutflowCount,"Pass-Trig");
        }
        ANA_MSG_DEBUG ("Checking trigger DONE");
    }

    // ############################################################################
    // Electronios
    // ############################################################################
    // const xAOD::ElectronContainerAux* elecsAux = 0;
    // ANA_CHECK(evtStore()->retrieve(elecs,elecsAux));
    // follow example https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_modify/
    const xAOD::ElectronContainer* elecs = 0;
    ANA_CHECK(evtStore()->retrieve(elecs,"Electrons"));
    auto shallowCopyElecs = xAOD::shallowCopyContainer(*elecs);
    std::unique_ptr<xAOD::ElectronContainer> shallowElecs (shallowCopyElecs.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> shallowAuxElecs (shallowCopyElecs.second);
    //
    ANA_MSG_DEBUG ("Making elec list obj:"<<elecs);
    m_elecEta->clear();
    m_elecPhi->clear();
    m_elecPt->clear();
    m_elecE->clear();
    m_elecQ->clear();
    std::vector<TLorentzVector*> elecVects;
    TLorentzVector* elecVect;
    // loop over electrinos
    // for (auto elec : * (content.muons)) {
    // for (const xAOD::Electron* elec : *elecs) {
    // for (auto elec : *elecs) {
    for (auto elec : *shallowElecs) {
        ANA_MSG_DEBUG ("Consider elec:"<<elec);
        // apply electron selection
        // if (!m_elecIdSelection->accept(elec)) continue;
        if (!m_leptonSelection->accept(*elec)) continue;
        if (abs(elec->eta()>m_elecSelectionMaxEta)) continue;
        m_elecEta->push_back(elec->eta());
        m_elecPhi->push_back(elec->phi());
        m_elecPt->push_back(elec->pt());
        m_elecE->push_back(elec->e());
        m_elecQ->push_back((float)elec->charge());
        // make TLorentzVector to store elec info
        elecVect = new TLorentzVector();
        elecVect->SetPtEtaPhiE(elec->pt(),elec->eta(),elec->phi(),elec->e());
        elecVects.push_back(elecVect);
        // make histograms
        hist("h_elecPt")->Fill(elec->pt()*0.001); // GeV
        hist("h_elecEta")->Fill(elec->eta());
        hist("h_elecPhi")->Fill(elec->phi());
    }

    // ############################################################################
    // Muons
    // ############################################################################
    // get muon container of interest
    // follow example https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_xaod_modify/
    const xAOD::MuonContainer* muons = 0;
    ANA_CHECK(evtStore()->retrieve(muons,"Muons"));
    auto shallowCopyMuons = xAOD::shallowCopyContainer(*muons);
    std::unique_ptr<xAOD::MuonContainer> shallowMuons (shallowCopyMuons.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> shallowAuxMuons (shallowCopyMuons.second);
    //
    ANA_MSG_DEBUG ("Making muon list");
    // const xAOD::MuonContainer* muons = 0;
    // ANA_CHECK(evtStore()->retrieve(muons,"Muons"));
    // loop over the muons in the container
    m_muonEta->clear();
    m_muonPhi->clear();
    m_muonPt->clear();
    m_muonE->clear();
    m_muonQ->clear();
    std::vector<TLorentzVector*> muonVects;
    TLorentzVector* muonVect;
    ANA_MSG_DEBUG ("Making muon list obj:"<<muons);
    // loop over muons
    // for (auto muon : *muons) {
    for (auto muon : *shallowMuons) {
        // apply muon selection
        ANA_MSG_DEBUG ("Consider muon:"<<muon);
        // if ( !m_muonSelection->accept(*muon) ) continue;
        // if (!m_leptonSelection->accept(muon)) continue;
        if (abs(muon->eta()>m_muonSelectionMaxEta)) continue;
        // push into vectors for tree output
        m_muonEta->push_back(muon->eta());
        m_muonPhi->push_back(muon->phi());
        m_muonPt->push_back(muon->pt());
        m_muonE->push_back(muon->e());
        m_muonQ->push_back((float)muon->charge());
        // make TLorentzVector to store muon info
        muonVect = new TLorentzVector();
        muonVect->SetPtEtaPhiE(muon->pt(),muon->eta(),muon->phi(),muon->e());
        muonVects.push_back(muonVect);
        // make histograms
        hist("h_muonPt")->Fill(muon->pt()*0.001); // GeV
        hist("h_muonEta")->Fill(muon->eta());
        hist("h_muonPhi")->Fill(muon->phi());
    }
    ANA_MSG_DEBUG ("Making muon list DONE");

    // ############################################################################
    // Jets
    // ############################################################################
    // get jet container of interest
    const xAOD::JetContainer* jets = 0;
    ANA_CHECK (evtStore()->retrieve( jets, "AntiKt4EMTopoJets"));
    ANA_MSG_DEBUG ("execute(): number of jets = " << jets->size());
    m_jetEta->clear();
    m_jetPhi->clear();
    m_jetPt->clear();
    m_jetE->clear();
    // loop over the jets in the container
    for (const xAOD::Jet* jet : *jets) {
        hist("h_jetPt")->Fill(jet->pt()*0.001); // GeV
        m_jetEta->push_back(jet->eta());
        m_jetPhi->push_back(jet->phi());
        m_jetPt->push_back(jet->pt());
        m_jetE->push_back(jet->e());
    }

    // ############################################################################
    // Do event selection
    // ############################################################################
    // Choose which leptons (muons, or electrons) to do event selection over
    std::vector<TLorentzVector*>* leptons;
    if (m_selectOnMuons) leptons = &muonVects;
    else leptons = &elecVects;

    // Event selection: dilepton pair in dilep range m_dilepMassLow->m_dilepMassHigh
    ANA_MSG_DEBUG ("Checking for Dilep candidate");
    double dilepMass;
    bool foundCandidate = false;
    for (unsigned int i=0; i<(*leptons).size(); i++){
        for (unsigned int j=0; j<(*leptons).size(); j++){
            // avoid using same charge muon (incl same muon...)
            if ((*m_muonQ)[i]==(*m_muonQ)[j]) continue;
            // calculate mass
            dilepMass = (*((*leptons)[i])+*((*leptons)[j])).Mag();
            dilepMass/=1000; // convert to GeV
            // evaluate if in range
            foundCandidate |= (dilepMass>=m_dilepMassLow && dilepMass<=m_dilepMassHigh);
        }
    }
    // skip events without Dilep candidate
    if (!foundCandidate){
        ANA_MSG_DEBUG ("execute(): Skipped on account of no Dilep candidate");
        return StatusCode::SUCCESS; // go to next event
    }
    else {
        hist("h_cutflow")->Fill(++cutflowCount);
        hist("h_cutflow")->GetXaxis()->SetBinLabel(cutflowCount,"Pass-Dilep-Cand");
    }

    // ############################################################################
    // Fill tree
    // ############################################################################

    // Fill the event into the tree:
    tree ("analysis")->Fill();
    nEventsPass+=1;
    hist("h_cutflow")->Fill(++cutflowCount);
    hist("h_cutflow")->GetXaxis()->SetBinLabel(cutflowCount,"Pass-All");
    return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.
    ANA_MSG_INFO ("==============================================================");
    ANA_MSG_INFO ("in finalize, nEvents = " << nEvents << ", nEventsPass = " << nEventsPass);
    return StatusCode::SUCCESS;
}

MyxAODAnalysis :: ~MyxAODAnalysis () {
    delete m_jetEta;
    delete m_jetPhi;
    delete m_jetPt;
    delete m_jetE;
}
