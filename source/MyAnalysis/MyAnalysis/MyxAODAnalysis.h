#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <TH1.h>
#include <xAODJet/JetContainer.h>
// Root
#include <TTree.h>
#include <TLorentzVector.h>
#include <vector>
// Muons
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
// Trigger
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
// Electron
// #include <xAODEgamma/ElectronContainer.h>
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
// #include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"//needed?
// #include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
// #include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
// #include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"

// lepton selection
#include <IsolationSelection/IsolationSelectionTool.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    ~MyxAODAnalysis();

private:
    // Configuration, and any other types of variables go here.
    //float m_cutValue;
    //TTree *m_myTree;
    //TH1 *m_myHist;

    // ###############################################################
    // GRL
    // ###############################################################
    double m_electronPtCut;
    std::string m_sampleName;
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl;
    int nEvents;
    int nEventsPass;
    double m_dilepMassLow;
    double m_dilepMassHigh;
    bool m_doTrigger;
    bool m_doGrl;
    bool m_selectOnMuons;

    // ###############################################################
    // variables for tree
    // ###############################################################
    unsigned int m_runNumber = 0; ///< Run number
    unsigned long long m_eventNumber = 0; ///< Event number
    /// Jet 4-momentum variables
    std::vector<float> *m_jetEta = nullptr;
    std::vector<float> *m_jetPhi = nullptr;
    std::vector<float> *m_jetPt = nullptr;
    std::vector<float> *m_jetE = nullptr;
    /// muon 4-momentum variables
    std::vector<float> *m_muonEta = nullptr;
    std::vector<float> *m_muonPhi = nullptr;
    std::vector<float> *m_muonPt = nullptr;
    std::vector<float> *m_muonE = nullptr;
    std::vector<float> *m_muonQ = nullptr;
    /// elec 4-momentum variables
    std::vector<float> *m_elecEta = nullptr;
    std::vector<float> *m_elecPhi = nullptr;
    std::vector<float> *m_elecPt = nullptr;
    std::vector<float> *m_elecE = nullptr;
    std::vector<float> *m_elecQ = nullptr;

    // ###############################################################
    // Elec selection
    // ###############################################################
    // AsgElectronLikelihoodTool* m_elecIdSelection;
    asg::AnaToolHandle<AsgElectronLikelihoodTool> m_elecIdSelection; 
    std::string m_elecWp;
    double m_elecSelectionMaxEta;

    asg::AnaToolHandle<CP::IsolationSelectionTool> m_leptonSelection;

    // ###############################################################
    // Muon selection tool
    // IMuonSelectionTool: https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces
    // IMuonCalibrationAndSmearingTool: https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces
    // ###############################################################
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; 
    asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; 
    double m_muonSelectionMaxEta;
    std::string m_muonWp;

    // ###############################################################
    // Trigger
    // ###############################################################
    asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
    asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool;
    std::string m_triggerPattern;

};

#endif
