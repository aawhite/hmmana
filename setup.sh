#!/bin/bash


# voms-proxy-init -voms atlas                                                     
# lsetup panda
# lsetup rucio

setupATLAS
lsetup panda

cd build
asetup 21.2.48,AnalysisBase
# asetup --restore
source x86_64-*/setup.sh
cd -

# needed for submitting to grid

echo ""
echo "Tutorial data:"
export ALRB_TutorialData="/afs/cern.ch/atlas/project/PAT/tutorial/cern-jan2018/"
echo $ALRB_TutorialData
